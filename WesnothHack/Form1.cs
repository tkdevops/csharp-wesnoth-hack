using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace WesnothHack
{
    public partial class Form1 : Form
    {
        const bool debug = true;

        int m_iAllWordCheck = 8;
        ArrayList m_arrData = null;
        ArrayList m_arrEditData = null;
        string strPath = System.IO.Directory.GetCurrentDirectory();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //strPath = @"D:\Program Files\Wesnoth\userdata\saves";//debug Test 
            string []strAllFiles = System.IO.Directory.GetFiles(strPath);
            for (int iCountFiles = 0; iCountFiles < strAllFiles.Length; iCountFiles++)
            {
                comboBox1.Items.Add(strAllFiles[iCountFiles].Replace(strPath + "\\",""));
            }
        }

        // load file
        private void button3_Click(object sender, EventArgs e)
        {
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;

            if (File.Exists(strPath + "\\" + comboBox1.Text))
            {
                m_arrData = new ArrayList();
                try
                {
                    StreamReader sr = File.OpenText(strPath + "\\" + comboBox1.Text);
                    string strTmp = "";
                    while ((strTmp = sr.ReadLine()) != null)
                    {
                        m_arrData.Add(strTmp);
                    }                    
                    sr.Close();
                    groupBox2.Enabled = true;
                    groupBox3.Enabled = true;

                    // find Turns and MaxTurns
                    string strTurns = searchAndGetValue("turn_at=\"");
                    if (strTurns != "")
                    {
                        numericUpDown10.Value = decimal.Parse(strTurns);
                        // get MaxTurns
                        int iLine = searchAndGetLine("turn_at=\"",0);
                        string strMaxTurns = searchAndGetValue("turns=\"", iLine);
                        if (strMaxTurns != "")
                        {
                            numericUpDown11.Value = decimal.Parse(strMaxTurns);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\r\nCan't read data..", "Error"
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        // value of that's line
        private string lineValue(int iLine)
        {
            string[] strTxt = ((string)m_arrData[iLine]).Split('=');
            return strTxt.Length > 1 ? strTxt[1].Trim().Replace("\"", "") : "";
        }

        // search Word and return value
        // use this method for UNIQUE word
        private string searchAndGetValue(string strWord)
        {
            return searchAndGetValue(strWord, 0);
        }
        private string searchAndGetValue(string strWord, int iLineStart)
        {
            int iLine = searchAndGetLine(strWord, iLineStart);
            if (iLine > -1)
            {
                return lineValue(iLine);
            }
            return "";
        }        

        // search word and return line of data
        private int searchAndGetLine(string strWord, int iStart)
        {
            int iCount = iStart;
            if (iCount < 0)
            {
                iCount = 0;
            }
            while (iCount < m_arrData.Count)
            {
                if (((string)m_arrData[iCount]).ToUpper().Replace(" ","").IndexOf(strWord.ToUpper()) >= 0)
                {
                    return iCount;
                }
                iCount++;
            }
            //m_arrData.IndexOf(strWord, iStart, iStop);
            return -1;
        }

        private void clearData()
        {
            textBox1.Text = "";       // name
            numericUpDown3.Value = 0; // hp
            numericUpDown12.Value = 0; // max hp
            numericUpDown4.Value = 0; // exp
            numericUpDown5.Value = 0; // max exp
            numericUpDown6.Value = 0; // lv
            numericUpDown7.Value = 0; // move remain
            numericUpDown8.Value = 0; // max move
        }

        private string saveData()
        {
            return "";
        }

        // find personal Data
        private void button1_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = false;
            string strX = "\tx=\"" + numericUpDown1.Value.ToString() + "\"";
            string strY = "\ty=\"" + numericUpDown2.Value.ToString() + "\"";

            int iLineX = 0;
            bool cmdExitSearchHero = false;
            while (!cmdExitSearchHero && iLineX >= 0)
            {
                m_arrEditData = new ArrayList(); // save data changed
                iLineX = searchAndGetLine(strX, iLineX);
                if (iLineX >= 0)
                {
                    int iLineY = searchAndGetLine(strY, iLineX);
                    if (iLineY >= 0 && iLineY - 1 == iLineX)
                    {
                        // reverse search
                        bool cmdExitSearchPersonData = false;
                        int iCountEditData = 0; // if = m_iAllWordCheck, it's = found all data
                        while (!cmdExitSearchPersonData && iLineX >= 0)
                        {
                            string strData = (string)m_arrData[iLineX];
                            if (strData.ToUpper().IndexOf("[") > -1)
                            {
                                iLineX = iLineY; // next location
                                clearData();
                                m_arrEditData = null; // clear editdata
                                cmdExitSearchPersonData = true;
                            }
                            else if (strData.ToLower().IndexOf("\tuser_description") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",user_description");
                                iCountEditData++;
                                textBox1.Text = lineValue(iLineX);
                                iLineX--; // decrese line for next search
                            }
                            else if (strData.ToLower().IndexOf("\thitpoints") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",hitpoints");
                                iCountEditData++;
                                numericUpDown3.Value = decimal.Parse(lineValue(iLineX));
                                iLineX--; // decrese line for next search
                            }
                            else if (strData.ToLower().IndexOf("\tmax_hitpoints") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",max_hitpoints");
                                iCountEditData++;
                                numericUpDown12.Value = decimal.Parse(lineValue(iLineX));
                                iLineX--; // decrese line for next search
                            }
                            else if (strData.ToLower().IndexOf("\texperience") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",experience");
                                iCountEditData++;
                                numericUpDown4.Value = decimal.Parse(lineValue(iLineX));
                                iLineX--; // decrese line for next search
                            }
                            else if (strData.ToLower().IndexOf("\tmax_experience") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",max_experience");
                                iCountEditData++;            
                                numericUpDown5.Value = decimal.Parse(lineValue(iLineX));
                                iLineX--; // decrese line for next search
                            }
                            else if (strData.ToLower().IndexOf("\tlevel") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",level");
                                iCountEditData++;
                                numericUpDown6.Value = decimal.Parse(lineValue(iLineX));
                                iLineX--; // decrese line for next search
                            }
                            // move remains
                            else if (strData.ToLower().IndexOf("\tmoves") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",moves");
                                iCountEditData++;
                                numericUpDown7.Value = decimal.Parse(lineValue(iLineX));
                                iLineX--; // decrese line for next search
                            }
                            else if (strData.ToLower().IndexOf("\tmax_moves") > -1)
                            {
                                // add key to arrayList [format : line,keyword]
                                m_arrEditData.Add(iLineX.ToString() + ",max_moves");
                                iCountEditData++;
                                numericUpDown8.Value = decimal.Parse(lineValue(iLineX));
                                iLineX--; // decrese line for next search
                            }
                            else
                            {
                                iLineX--; // decrese line                            
                            }

                            // exit when equal all...
                            if (iCountEditData == m_iAllWordCheck)
                            {
                                cmdExitSearchPersonData = true;
                                cmdExitSearchHero = true;
                            }
                        }
                        if (iLineX <= 0) iLineX = iLineY; // safe unlimited loop..
                    }
                    else
                    {
                        iLineX++;
                    }
                }// check iLineX
            }

            if (iLineX < 0 || !cmdExitSearchHero)
            {
                MessageBox.Show("hero not found..", "Information", MessageBoxButtons.OK
                , MessageBoxIcon.Information);
            }
            else
            {
                groupBox4.Enabled = true;
            }
        }

        // save
        private void button2_Click(object sender, EventArgs e)
        {
            // change data in memory
            for (int iCountMem = 0; iCountMem < m_arrEditData.Count; iCountMem++)
            {
                string []strDetail = ((string)m_arrEditData[iCountMem]).Split(',');
                int iLine = int.Parse(strDetail[0].Trim());
                string []strSplitData = ((string)m_arrData[iLine]).Split('=');
                switch(strDetail[1])
                {
                    case "user_description":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + textBox1.Text + "\"";
                        break;
                    case "hitpoints":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + numericUpDown3.Value.ToString() + "\"";
                        break;
                    case "max_hitpoints":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + numericUpDown12.Value.ToString() + "\"";
                        break;
                    case "experience":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + numericUpDown4.Value.ToString() + "\"";
                        break;
                    case "max_experience":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + numericUpDown5.Value.ToString() + "\"";
                        break;
                    case "level":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + numericUpDown6.Value.ToString() + "\"";
                        break;
                    case "moves":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + numericUpDown7.Value.ToString() + "\"";
                        break;
                    case "max_moves":
                        m_arrData[iLine] = strSplitData[0] + "=\"" + numericUpDown8.Value.ToString() + "\"";
                        break;
                }
            }

            // save to file
            string strFilename = strPath + "\\" + comboBox1.Text;
            if (File.Exists(strFilename))
            {
                if (MessageBox.Show("Replace.. ?", "File exist..", MessageBoxButtons.YesNo
                , MessageBoxIcon.Asterisk) != DialogResult.Yes)
                {
                    MessageBox.Show("You can change file's name at SaveName box.."
                    , "Information..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            StreamWriter sw = new StreamWriter(strFilename);
            for (int iCountLine = 0; iCountLine < m_arrData.Count; iCountLine++)
            {
                sw.WriteLine((string)m_arrData[iCountLine]);
            }
            sw.Close();
            MessageBox.Show("save file complete.."
                    , "Information..", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you can mail me at..\r\nBenNueng@gmail.com (^^)"
                    , "About..", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}